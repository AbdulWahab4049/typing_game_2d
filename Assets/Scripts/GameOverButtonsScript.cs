﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverButtonsScript : MonoBehaviour {

	public void OnRestartClick ()
    {
        Stats.restartPerformed = true;
        string currentLevelName = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(currentLevelName);
	}
	
	public void OnMainMenuClick ()
    {
		Sound.resetSettings();
		SceneManager.LoadScene("MainMenu");
	}
}
