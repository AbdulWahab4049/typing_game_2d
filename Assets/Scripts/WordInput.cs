using UnityEngine;
using System;

public class WordInput : MonoBehaviour
{
    private GameManager wordManager;
    private CapsLockDetector capsLockDetector;
    private bool isCapsLockOn = false;

    private static readonly KeyCode BLUE_ROCKET_TRIGGER = KeyCode.Tab;
    private static readonly string YELLOW_ROCKET_TRIGGER = " ";
    private static readonly string GREEN_ROCKET_TRIGGER = "\b";

    void Start()
    {
        wordManager = GetComponent<GameManager>();
        capsLockDetector = GetComponent<CapsLockDetector>();
    }

    void Update()
    {
        foreach (char letter in Input.inputString)
        {
            Debug.Log((int)letter);
        }


        if (!(wordManager.isGameOver || wordManager.isLevelComplete))
        {
            HandleInstructionsPanel();

            HandleRocketTriggering();

            foreach (char letter in Input.inputString)
            {
                HandleCapsLockPrompt(letter);

                wordManager.TypeLetter(letter);
            }
        }
    }


    private void HandleInstructionsPanel()
    {
        if (Input.inputString.EndsWith("\r"))
        {
            if (wordManager.isGamePaused)
            {
                wordManager.UnpauseGameScene();
                wordManager.HideInstructionsPanel();
            }
            else
            {
                wordManager.PauseGameScene();
                wordManager.ShowInstructionsPanel();
            }
            return;
        }
    }

    private void HandleRocketTriggering()
    {
        if (Input.GetKeyDown(BLUE_ROCKET_TRIGGER))
        {
            wordManager.TriggerRocketAction("blue");
        }

        if (Input.inputString.EndsWith(YELLOW_ROCKET_TRIGGER))
        {
            wordManager.TriggerRocketAction("yellow");
        }

        if (Input.inputString.EndsWith(GREEN_ROCKET_TRIGGER))
        {
            wordManager.TriggerRocketAction("green");
        }
    }

    private void HandleCapsLockPrompt(char letter)
    {
        if (wordManager.isGamePaused)
        {
            return;
        }

        if ((Char.IsUpper(letter) && !IsShiftPressed())
        || (Char.IsLower(letter) && IsShiftPressed()))
        {
            capsLockDetector.SetCapsLockOn();
        }
    }

    private bool IsShiftPressed()
    {
        return Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
    }
}