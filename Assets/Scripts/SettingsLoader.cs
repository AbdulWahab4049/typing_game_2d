﻿using UnityEngine.SceneManagement;
using UnityEngine;
using System.Collections;
/// Loads the life, number of words required to complete level, the actual 
/// words to appear in each leveln min and max rock speeds 
/// Uses URL https://www.asteroidtypinggame.com/getLevelInfo.php to load
public static class SettingsLoader
{
    public static IEnumerator ReadLevelData()
    {
        LevelData.Reset();

        WWWForm form = new WWWForm();
        string levelName = SceneManager.GetActiveScene().name;
        form.AddField("levelName", levelName);

        WWW request = new WWW("https://www.asteroidtypinggame.com/getLevelInfo.php", form);
        yield return request;

        if (request.text != null && request.text[0] == '0')
        {
            string[] levelData = request.text.Split('\t');
            LevelData.life = int.Parse(levelData[1]);
            Stats.wordsToComplete = int.Parse(levelData[2]);
            string combinedWords = levelData[3];

            LevelData.minSpeed = int.Parse(levelData[4]);
            LevelData.maxSpeed = int.Parse(levelData[5]);

            foreach (string line in combinedWords.Split('&'))
            {
                string[] wordsInLine = line.Split(' ');
                LevelData.totalWordsCount += GetNumberOfWordsInLine(wordsInLine);
                LevelData.WordPairs.Add(wordsInLine);
            }
        }
        else
        {
            Debug.Log("Problem fetching Level Data from server: https://www.asteroidtypinggame.com/getLevelInfo.php");
        }
    }

    private static int GetNumberOfWordsInLine(string[] line)
    {
        int count = 0;
        foreach (string word in line)
        {
            if (!word.Contains("Rocket"))
            {
                count++;
            }
        }
        return count;
    }
}
