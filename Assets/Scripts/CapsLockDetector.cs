using UnityEngine;
using System.Runtime.InteropServices;


public class CapsLockDetector : MonoBehaviour
{
    private GameObject capsLockStatusTextReference;
    private GameManager gameManager;
    private bool capsLockPressed = false;

    void Start()
    {
        gameManager = GetComponent<GameManager>();
        capsLockStatusTextReference = GameObject.Find("Caps Lock Status");
        capsLockStatusTextReference.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.CapsLock))
        {
            capsLockPressed = !capsLockPressed;
        }

        if (gameManager.isGameOver || gameManager.isLevelComplete || gameManager.isGamePaused)
        {
            capsLockStatusTextReference.SetActive(false);
        }
        else
        {
            capsLockStatusTextReference.SetActive(capsLockPressed);
        }
    }

    public GameObject GetCapsLockStatusTextReference()
    {
        return capsLockStatusTextReference;
    }

    public bool isCapsLockOn()
    {
        return capsLockPressed;
    }

   public void SetCapsLockOn()
    {
        capsLockPressed = true;
    }
}