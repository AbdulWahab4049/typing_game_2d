﻿using System.Collections;

public static class LevelData
{
    private static ArrayList wordPairs = new ArrayList();

    public static int life;
    public static int totalWordsCount;
    public static int minSpeed;
    public static int maxSpeed;
    public static int points;
    public static int blueRocketCount;
    public static int greenRocketCount;
    public static int yellowRocketCount;

    public static void Reset()
    {
        life = totalWordsCount = minSpeed = maxSpeed = points = 0;
        yellowRocketCount = blueRocketCount = greenRocketCount = 0; 
        WordPairs.Clear();
    }

    public static ArrayList WordPairs
    {
        get
        {
            return wordPairs;
        }
        set
        {
            wordPairs = value;
        }
    }
}
