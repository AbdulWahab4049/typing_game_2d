﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelIndicatorsLoader : MonoBehaviour {

    void OnEnable()
    {
        //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    private void OnLevelFinishedLoading(Scene scene, LoadSceneMode loadSceneMode)
    {
        if (scene.name.StartsWith("Level"))
        {
            this.GetComponent<TextMeshProUGUI>().text = scene.name;
            SetLevelUIVariables();
            Stats.ResetStats();
        }
    }

    private void SetLevelUIVariables()
    {
        if (!Stats.restartPerformed)
        { 
            Stats.blueRocketCount += LevelData.blueRocketCount;
            Stats.yellowRocketCount += LevelData.yellowRocketCount;
            Stats.greenRocketCount += LevelData.greenRocketCount;
            Stats.points += Stats.perLevelPoints;
        }

        TextMeshProUGUI blueRocketIndicator = GameObject.Find("blueRocketIndicator").GetComponent<TextMeshProUGUI>();
        blueRocketIndicator.SetText(Stats.blueRocketCount.ToString());

        TextMeshProUGUI greenRocketIndicator = GameObject.Find("greenRocketIndicator").GetComponent<TextMeshProUGUI>();
        greenRocketIndicator.SetText(Stats.greenRocketCount.ToString());

        TextMeshProUGUI yellowRocketIndicator = GameObject.Find("yellowRocketIndicator").GetComponent<TextMeshProUGUI>();
        yellowRocketIndicator.SetText(Stats.yellowRocketCount.ToString());

        GameObject.Find("Points Value").GetComponent<TextMeshProUGUI>().text = Stats.points.ToString();
    }
}
