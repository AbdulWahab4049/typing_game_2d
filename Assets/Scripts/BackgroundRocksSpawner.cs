﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class BackgroundRocksSpawner : MonoBehaviour
{
    private const string SPAWN_MEDIUM_SIZED_ROCKS_PATTERN = "SpawnRocksPattern";
    private const string SPAWN_ROTATING_ROCK = "SpawnRotatingRock";
    private const int SCREEN_RIGHT_FACTOR = 1400;
    private const float ROCK_PATTERN_FIRST_TIME_DURATION = 0f;
    private const float ROCK_PATTERN_REPEAT_DURATION = 60f;
    private const float ROTATING_ROCK_DURATION = 30f;
    public GameObject[] backgroundRocks;
    public GameObject[] rotatingRocks;
    public GameObject largeSizedRockPattern;
    public RectTransform canvas;
    private GameManager gameManager;
    private bool firstTime = true;
    private int backgroundRocksSpawnCount = 0;

    private void Start()
    {
        gameManager = GetComponent<GameManager>();
    }

    void OnEnable()
    {
        //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        StartSpawning();
    }

    public void StartSpawning()
    {
        SpawnRocksPattern();
        firstTime = false;
        InvokeRepeating(SPAWN_MEDIUM_SIZED_ROCKS_PATTERN, ROCK_PATTERN_FIRST_TIME_DURATION, ROCK_PATTERN_REPEAT_DURATION);
        InvokeRepeating(SPAWN_ROTATING_ROCK, ROTATING_ROCK_DURATION, ROTATING_ROCK_DURATION);
    }

    void SpawnRotatingRock()
    {
        //meteorSpawner.SpawnSpaceRock(GetRotatingRockToSpawn());


        GameObject spawnedRotatingRock = Instantiate(GetRotatingRockToSpawn(), canvas);
        spawnedRotatingRock.transform.position = new Vector3(spawnedRotatingRock.transform.position.x,
            MeteorSpawner.CalculateYPosition());


        //float yPosition = Random.Range(MeteorSpawner.screenTopLimit - 
        //    getPercentage(MeteorSpawner.screenTopLimit - MeteorSpawner.screenBottomLimit, MeteorSpawner.SPAWN_TOP_PERCENTAGE)
        //    , MeteorSpawner.screenBottomLimit + 
        //    getPercentage(MeteorSpawner.screenTopLimit - MeteorSpawner.screenBottomLimit, MeteorSpawner.SPAWN_BOTTOM_PERCENTAGE));
        //spawnedRotatingRock.transform.position = new Vector3(MeteorSpawner.screenRightLimit + 30, yPosition);
    }

    void SpawnRocksPattern()
    {
        GameObject spaceRocksPattern;
        GameObject rockToSpawn = GetRockPatternToSpawn();

        if (firstTime)
        {
            spaceRocksPattern = Instantiate(rockToSpawn, canvas);
        }
        else
        {
            spaceRocksPattern = InstantiateToTheRightOfTheScreen(rockToSpawn, canvas);
        }

        backgroundRocksSpawnCount++;
    }

    private GameObject InstantiateToTheRightOfTheScreen(GameObject rockToSpawn, RectTransform canvas)
    {
        GameObject spaceRocksPattern = Instantiate(rockToSpawn, canvas);
        RectTransform spaceRockPatternTransform = spaceRocksPattern.GetComponent<RectTransform>();
        spaceRockPatternTransform.position = new Vector3(SCREEN_RIGHT_FACTOR ,spaceRockPatternTransform.position.y);
        return spaceRocksPattern;
    }

    private GameObject GetRotatingRockToSpawn()
    {
        int index = Random.Range(0, rotatingRocks.Length);
        return rotatingRocks[index];
    }

    private GameObject GetRockPatternToSpawn()
    {
        if (IsTheTenthTimeInSeries())
        {
            return largeSizedRockPattern;
        }
        else
        {
            int index = Random.Range(0, backgroundRocks.Length);
            return backgroundRocks[index];
        }
    }

    private bool IsTheTenthTimeInSeries()
    {
        return (backgroundRocksSpawnCount + 1) % 10 == 0;
    }

    private float getPercentage(float of, float percent)
    {
        return (of / 100) * percent;
    }
}