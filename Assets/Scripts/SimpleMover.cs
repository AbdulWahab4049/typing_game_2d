﻿using UnityEngine;

public class SimpleMover : MonoBehaviour
{
    private float leftMargin = 100;
    public float minSpeed = 200;
    public float maxSpeed = 300;
    private static float actualSpeed;
    private float screenLeftLimit;
    private const float LEFT_BOUND_FACTOR = 1.05f;

    private static bool isYellowRocketActivated;

    void Start()
    {
        screenLeftLimit = GameObject.Find("Canvas").GetComponent<RectTransform>().offsetMin.x;
        actualSpeed = Random.Range(minSpeed, maxSpeed);
    }

    void Update()
    {
        if (transform.position.x > screenLeftLimit * LEFT_BOUND_FACTOR - leftMargin)
            transform.Translate(Vector2.left * actualSpeed * Time.deltaTime, Space.World);
        else
        {
            Destroy(gameObject);
        }
    }

    public static void SlowActualSpeedToHalf()
    {
        actualSpeed /= 2;
    }

    public static void IncreaseActualSpeedToDouble()
    {
        actualSpeed *= 2;
    }

    public static bool IsYellowRocketActivated()
    {
        return isYellowRocketActivated;
    }

    public static void SetYellowRocketActivated(bool isActivated)
    {
        isYellowRocketActivated = isActivated;
    }
}
