using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

/**
 * Class managing word functionality as well as progress and life bars and game over 
 */
public class GameManager : MonoBehaviour
{
    public AudioClip[] audioClips;

    public SimpleHealthBar lifeBar;
    public SimpleHealthBar progressBar;

    private GameObject nextLevelPanel;
    private GameObject gameOverPanel;
    private GameObject instructionsPanel;

    public GameObject blueRocket;
    public GameObject greenRocket;
    public GameObject yellowRocket;

    public GameObject astronaught;
    public GameObject spaceStation;

    public TextMeshProUGUI blueRocketIndicator;
    public TextMeshProUGUI greenRocketIndicator;
    public TextMeshProUGUI yellowRocketIndicator;
    public GamePause pauseGame;

    public GameObject blueRocketCollectAnimation;

    private List<Word> words;
    private List<Word> rocketWords;

    private Word activeWord;
    private Word activeRocketWord;
    private AudioSource audioSource;

    private MeteorMover meteorMover;

    private bool hasActiveWord;
    private bool hasActiveRocketWord;

    public bool isLevelComplete, isGameOver, isGamePaused;
    private bool isDoublePoints;

    private void Awake()
    {
        nextLevelPanel = GameObject.Find("Level Complete Panel");
        gameOverPanel = GameObject.Find("Game Over Panel");
        instructionsPanel = GameObject.Find("Instructions Panel");

        nextLevelPanel.SetActive(false);
        gameOverPanel.SetActive(false);
        instructionsPanel.SetActive(false);

        GameObject.Find("Main Camera").AddComponent<CapsLockDetector>();

        Cursor.visible = false;
    }

    void Start()
    {
        //StartCoroutine(SettingsLoader.ReadLevelData());

        //Dummy Values
        LevelData.life = 5;
        Stats.wordsToComplete = Stats.totalWordCount = 5;


        //UpdateProgressBar();
        InitializeLifeBar(0);
        audioSource = gameObject.GetComponent<AudioSource>();
        words = new List<Word>();
        rocketWords = new List<Word> { new Word("blue",null), new Word("red", null), new Word("green", null), new Word("yellow", null) };
        if (Stats.restartPerformed)
        {
            Stats.restartPerformed = false;
        }
    }

    public void PauseGameScene()
    {
        Cursor.visible = true;
        Time.timeScale = 0;
        isGamePaused = true;
    }

    public void UnpauseGameScene()
    {
        Cursor.visible = false;
        Time.timeScale = 1;
        isGamePaused = false;
    }

    public void ShowInstructionsPanel()
    {
        instructionsPanel.SetActive(true);
    }

    public void onBackToMainMenuClickFromInGameInstructions() 
    {
        if (isGamePaused)
        {
            UnpauseGameScene();
        }
        Sound.resetSettings();
        Stats.lifeLeft = 0;
        SceneManager.LoadScene("MainMenu");
	}

    public void HideInstructionsPanel()
    {
        instructionsPanel.SetActive(false);
    }

    public void AddWord(Word word)
    {
        words.Add(word);
    }

    public void RemoveWord(string text)
    {
        for (int i = 0; i < words.Count; ++i)
        {
            if (words[i].GetText() == text)
            {
                words[i].setDestroyed(true);
                words.RemoveAt(i);
                break;
            }
        }
    }

    internal string GetActiveWordText()
    {
        if (activeWord != null)
        { 
            return activeWord.GetText();
        }
        return null;
    }

    internal void ResetActiveWord()
    {
        words.Remove(activeWord);
        hasActiveWord = false;
        activeWord = null;
    }

    internal void ResetActiveRocketWord()
    {
        hasActiveRocketWord = false;
        activeRocketWord = null;
    }

    internal AudioClip GetRandomRockDestroySound()
    {
        return audioClips[UnityEngine.Random.Range(1, audioClips.Length)];
    }

    internal void PlayAudioClip(AudioClip audioClipToPlay)
    {
        if (Sound.getSoundEffectEnabled())
        {
            audioSource.clip = audioClipToPlay;
            audioSource.Play();
        }
    }

    internal void PlayRockMissedAudioClip()
    {
        if (Sound.getSoundEffectEnabled())
        {
            audioSource.clip = audioClips[0];
            audioSource.Play();
        }
    }

    public void TypeLetter(char letter)
    {
        Stats.totalLettersTyped++;
        
        if (hasActiveWord || hasActiveRocketWord)
        {
            //type a word or rocket letter if eligible    
            if (hasActiveWord && activeWord.GetNextLetter() == letter)
            {
                //type word's letter
                activeWord.TypeLetter();
                Stats.letterHits++;
            }

            if (hasActiveRocketWord && activeRocketWord.GetNextLetter() == letter && IsRocketPresentOnScreen(activeRocketWord))
            {
                //type rocket's letter
                activeRocketWord.TypeLetter();
                Stats.letterHits++;
            }
        }
        else
        {
            //find next active and type its letter if eligible
            FindWordFromListAndTypeLetterOfMatchingActiveWord(letter);
        }

        //if we have completely typed the active word
        if (hasActiveWord && activeWord.WordTyped())
        {
            activeWord.RemoveWord();
            hasActiveWord = false;
            words.Remove(activeWord);
            Stats.wordsTyped++;
            UpdateProgressBar();

            if (isDoublePoints && words.Count > 0)
            {
                System.Random random = new System.Random();
                int randomIndex = random.Next(words.Count);
                words[randomIndex].RemoveWord();
                words.Remove(words[randomIndex]);
                Stats.wordsTyped++;
                UpdateProgressBar();
            }

            if (Stats.wordsTyped >= Stats.wordsToComplete)
            {
                NextLevel();
            }

        }

        //if the rocket word has been completely typed
        if (hasActiveRocketWord && activeRocketWord.WordTyped())
        {
            activeRocketWord.RemoveWord();
            hasActiveRocketWord = false;
            rocketWords.Remove(activeRocketWord);
            CollectRocket(activeRocketWord);
        }
    }

    internal void TriggerAstronaught()
    {
        astronaught.GetComponent<LeftToRightMover>().enabled = true;
    }

    internal void TriggerSpaceStation()
    {
        spaceStation.GetComponent<LeftToRightMover>().enabled = true;
    }

    private void FindWordFromListAndTypeLetterOfMatchingActiveWord(char letter)
    {
        bool letterTyped = false;

        foreach (Word word in words)
        {
            if (word.GetNextLetter() == letter && !word.IsDestroyed())
            {
                activeWord = word;
                hasActiveWord = true;
                word.TypeLetter();
                Stats.letterHits++;
                letterTyped = true;
                break;
            }
        }

        if (letterTyped)
        {
            Debug.Log("Currently active word: " + activeWord.GetText());
            return;
        }

        foreach (Word rocketWord in rocketWords)
        {
            if (rocketWord.GetNextLetter() == letter)
            {
                if (IsRocketPresentOnScreen(rocketWord))
                {
                    activeRocketWord = rocketWord;
                    hasActiveRocketWord = true;
                    rocketWord.TypeLetter();
                    Stats.letterHits++;
                    break;
                }
            }
        }
    }

    private void CollectRocket(Word activeRocketWord)
    {
        switch (activeRocketWord.GetText())
        {
            case "blue":
                CollectBlueRocket();
                break;
            case "green":
                CollectGreenRocket();
                break;
            case "yellow":
                CollectYellowRocket();
                break;
            default:
                break;
        }
    }

    private bool IsRocketPresentOnScreen(Word activeRocketWord)
    {
        GameObject activeRocket;
        switch (activeRocketWord.GetText())
        {
            case "blue":
                activeRocket = blueRocket;
                break;
            case "green":
                activeRocket = greenRocket;
                break;
            case "yellow":
                activeRocket = yellowRocket;
                break;
            default:
                return false;
        }
        return activeRocket.GetComponent<RocketMover>() != null && activeRocket.GetComponent<RocketMover>().IsPresentOnScreen();
    }

    internal void TriggerGreenRocket()
    {
        greenRocket.GetComponent<RocketMover>().enabled = true;
    }

    internal void TriggerYellowRocket()
    {
        yellowRocket.GetComponent<RocketMover>().enabled = true;
    }

    internal void TriggerBlueRocket()
    {
        blueRocket.GetComponent<RocketMover>().enabled = true;
    }

    internal void CollectBlueRocket()
    {
        Destroy(blueRocket);
        LevelData.blueRocketCount += 1;
        UpdateBlueRocketCountOnUI();
    }

    internal void CollectGreenRocket()
    {
        Destroy(greenRocket);
        LevelData.greenRocketCount += 1;
        UpdateGreenRocketCountOnUI();
    }

    internal void CollectYellowRocket()
    {
        Destroy(yellowRocket);
        LevelData.yellowRocketCount += 1;
        UpdateYellowRocketCountOnUI();
    }

    private void UpdateBlueRocketCountOnUI()
    {
        blueRocketIndicator.text = (Stats.blueRocketCount + LevelData.blueRocketCount).ToString();
    }

    private void UpdateYellowRocketCountOnUI()
    {
        yellowRocketIndicator.text = (Stats.yellowRocketCount + LevelData.yellowRocketCount).ToString();
    }

    private void UpdateGreenRocketCountOnUI()
    {
        greenRocketIndicator.text = (Stats.greenRocketCount + LevelData.greenRocketCount).ToString();
    }

    internal void TriggerRocketAction(string rocket)
    {
        if (isGameOver || isLevelComplete)
        {
            return;
        }

        if (rocket == null)
        {
            throw new ArgumentNullException(nameof(rocket));
        }

        switch (rocket)
        {
            case "blue":
                ActivateBlueRocketAction();
                break;
            case "green":
                ActivateGreenRocketAction();
                break;
            case "yellow":
                ActivateYellowRocketAction();
                break;
            default:
                break;
        }
    }

    private void ActivateBlueRocketAction()
    {
        if (IsBlueRocketPresent())
        {
            FreezeScreen();
            NegateBlueRocket();
            //InstantiateAnimation(blueRocketCollectAnimation);
        }
    }

    private void InstantiateAnimation(GameObject blueRocketCollectAnimation)
    {
        Instantiate(blueRocketCollectAnimation,
            new Vector3(blueRocket.transform.position.x - 20,
            blueRocket.transform.position.y,
            blueRocket.transform.position.z), 
            Quaternion.identity);
    }

    private void ActivateGreenRocketAction()
    {
        if (IsGreenRocketPresent())
        {
            SetDoublePoints(true);
            NegateGreenRocket();
        }
    }

    private void ActivateYellowRocketAction()
    {
        if (IsYellowRocketPresent())
        {
            NegateYellowRocket();
            StartCoroutine(SloMoForFifteenSec());
        }
    }

    private bool IsBlueRocketPresent()
    {
        return (Stats.blueRocketCount + LevelData.blueRocketCount) > 0;
    }

    private bool IsGreenRocketPresent()
    {
        return (Stats.greenRocketCount + LevelData.greenRocketCount) > 0;
    }

    private bool IsYellowRocketPresent()
    {
        return (Stats.yellowRocketCount + LevelData.yellowRocketCount) > 0;
    }

    private void FreezeScreen()
    {
        List<Word> wordsToComplete = new List<Word>(words.ToArray());
        //pauseGame.Freeze(0.5f);
        foreach (Word word in wordsToComplete)
        {
            foreach (char letter in word.GetText())
            {
                TypeLetter(letter);
            }
        }

        if (Stats.wordsTyped >= Stats.wordsToComplete)
        {
            NextLevel();
        }
    }

    IEnumerator SloMoForFifteenSec()
    {
        MeteorMover.SetYellowRocketActivated(true);
        MeteorMover.SlowActualSpeedToHalf();

        LeftToRightMover.SetYellowRocketActivated(true);
        LeftToRightMover.SlowActualSpeedToHalf();

        yield return new WaitForSeconds(15);

        ResumeNormalSpeed();
    }

    private void SetDoublePoints(bool value)
    {
        isDoublePoints = value;
    }

    private static void ResumeNormalSpeed()
    {
        if (MeteorMover.IsYellowRocketActivated())
        {
            MeteorMover.SetYellowRocketActivated(false);
            MeteorMover.IncreaseActualSpeedToDouble();
        }

        if (LeftToRightMover.IsYellowRocketActivated())
        {
            LeftToRightMover.SetYellowRocketActivated(false);
            LeftToRightMover.IncreaseActualSpeedToDouble();
        }
    }

    private void NegateBlueRocket()
    {
        LevelData.blueRocketCount -= 1;
        blueRocketIndicator = GameObject.Find("blueRocketIndicator").GetComponent<TextMeshProUGUI>();
        UpdateBlueRocketCountOnUI();
    }

    private void NegateGreenRocket()
    {
        LevelData.greenRocketCount -= 1;
        greenRocketIndicator = GameObject.Find("greenRocketIndicator").GetComponent<TextMeshProUGUI>();
        UpdateGreenRocketCountOnUI();
    }

    private void NegateYellowRocket()
    {
        LevelData.yellowRocketCount -= 1;
        yellowRocketIndicator = GameObject.Find("yellowRocketIndicator").GetComponent<TextMeshProUGUI>();
        UpdateYellowRocketCountOnUI();
    }

    public void InitializeLifeBar(double life)
    {
        if (life > Stats.totalWordCount)
        {
            life = Stats.totalWordCount;
        }

        if (!Stats.restartPerformed)
        {
            Stats.lifeLeft += (int)life;
            LevelData.life = Stats.lifeLeft;
        }
        else
        {
            Stats.lifeLeft = (int)life;
        }

        lifeBar.UpdateBar(Stats.lifeLeft, LevelData.life);
    }

    public void InitializeProgressBar(int arraySize)
    {
        Stats.totalWordCount = arraySize;
        if (Stats.wordsToComplete > Stats.totalWordCount)
        {
            Stats.wordsToComplete = Stats.totalWordCount;
        }
        progressBar.UpdateBar(Stats.wordsTyped, Stats.wordsToComplete);
        Stats.wordsTyped = Stats.wordsMissed = 0;
    }

    internal void UpdateLifeBar()
    {
        if (isGameOver || isLevelComplete)
        {
            return;
        }

        Stats.lifeLeft--;
        Stats.wordsMissed++;
        lifeBar.UpdateBar(Stats.lifeLeft, LevelData.life);
        CallGameOverIfRequired();
    }

    public void CallGameOverIfRequired()
    {
        if ((Stats.lifeLeft <= 0 || (Stats.wordsTyped + Stats.wordsMissed >= Stats.totalWordCount)) && !isGameOver)
        {
            Stats.CalculateStats();
            GameOver();
        }
    }

    internal void GameOverOnRedRockMissed()
    {
        Stats.lifeLeft = 0;
        Stats.wordsMissed++;
        lifeBar.UpdateBar(Stats.lifeLeft, LevelData.life);
        Stats.CalculateStats();
        GameOver();
    }

    internal void LevelCompleteOnWhiteRockTyped()
    {
        // minus 1 because white rock word will be incremented later
        Stats.wordsTyped = Stats.wordsToComplete - 1;
    }

    public void UpdateProgressBar()
    {
        progressBar.UpdateBar(Stats.wordsTyped, Stats.wordsToComplete);

        if (Stats.wordsTyped + Stats.wordsMissed >= Stats.totalWordCount)
        {
            if ( Stats.wordsTyped < Stats.wordsToComplete)
            {
                GameOver();
            }
            else if (Stats.wordsTyped >= Stats.wordsToComplete)
            {
                NextLevel();
            }     
        }
    }

    public void NextLevel()
    {
        Cursor.visible = true;
        if (!isGameOver)
        {
            if (MeteorMover.IsYellowRocketActivated())
            {
                ResumeNormalSpeed();
            }

            if (isDoublePoints)
            {
                isDoublePoints = false;
            }

            Stats.CalculateStats();
            SetCorrectLevelName();
            nextLevelPanel.SetActive(true);
            Stats.DisplayStats();
            isLevelComplete = true;
        }
    }

    private void SetCorrectLevelName()
    {
        nextLevelPanel.gameObject.transform.Find("Level Complete Title").GetComponent<TextMeshProUGUI>().
            SetText(SceneManager.GetActiveScene().name + " Complete");
    }

    public void GameOver()
    {
        Cursor.visible = true;
        if (!isLevelComplete)
        {
            if (MeteorMover.IsYellowRocketActivated())
            {
                ResumeNormalSpeed();
            }

            if (isDoublePoints)
            {
                isDoublePoints = false;
            }

            Stats.CalculateStats();
            gameOverPanel.SetActive(true);
            Stats.DisplayStats();
            isGameOver = true;
        }
    }
}
