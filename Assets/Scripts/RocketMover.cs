﻿using UnityEngine;

public class RocketMover : MonoBehaviour {

    private float screenRightLimit, screenLeftLimit;
    private float actualSpeed;
    private const float RIGHT_BOUND_FACTOR = 1.05f;
    private bool isPresentOnScreen;
    private GameManager gameManager;

    // Use this for initialization
    void Start () {
        screenRightLimit = GameObject.Find("Canvas").GetComponent<RectTransform>().offsetMax.x;
        screenLeftLimit = GameObject.Find("Canvas").GetComponent<RectTransform>().offsetMin.x;
        gameManager = GameObject.Find("Main Camera").GetComponent<GameManager>();
        actualSpeed = Random.Range(LevelData.minSpeed, LevelData.maxSpeed);
    }

    void Update()
    {

        if (transform.position.x < screenRightLimit * RIGHT_BOUND_FACTOR)
        {
            transform.Translate(Vector2.right * actualSpeed * Time.deltaTime, Space.World);
            if (transform.position.x > screenLeftLimit)
            {
                isPresentOnScreen = true;
            }
        }
        else
        {
            isPresentOnScreen = false;
            gameManager.ResetActiveRocketWord();
            Destroy(gameObject);
        }
    }

    public bool IsPresentOnScreen()
    {
        return isPresentOnScreen;
    }
}
