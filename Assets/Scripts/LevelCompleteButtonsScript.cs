﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelCompleteButtonsScript : MonoBehaviour
{
    public void OnNextLevelClick()
    {
        Cursor.visible = false;
        string currentLevelName = SceneManager.GetActiveScene().name;
        string nextLevelName = "Level " + (Convert.ToInt32(currentLevelName.Split(' ')[1]) + 1);
        SceneManager.LoadScene(nextLevelName);
    }

    public void OnRestartClick()
    {
        Stats.restartPerformed = true;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
