﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public sealed class TimeController : MonoBehaviour {

    public int Minutes;
    public int Seconds;
    public Text Time;
    private string[] timeSections;

	// Use this for initialization
	void Start () {

        Time.text = ConvertToDigital(Minutes.ToString());

        Time.text += ":";

        Time.text += ConvertToDigital(Seconds.ToString());

        timeSections = Time.text.Split(':');

        InvokeRepeating("TickTime",0.0f,1.0f);
	}

    private void TickTime()
    {
        
        if (timeSections[1] != "00")
        {
            Time.text = ConvertToDigital(timeSections[0]) + ":" + ConvertToDigital((Convert.ToInt32(timeSections[1]) - 1).ToString());
        }
        else if (timeSections[1] == "00")
        {
            Time.text = ConvertToDigital((Convert.ToInt32(timeSections[0]) - 1).ToString()) + ":" + "59";
        }
        timeSections = Time.text.Split(':');
    }

    void Update()
    {
        if (timeSections[0] == "00" && timeSections[1] == "00")
            CancelInvoke();
    }

    private string ConvertToDigital(string val){
        return (val.Length < 2 ? "0" + val : val);
    }

}
