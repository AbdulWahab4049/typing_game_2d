﻿using UnityEngine;
using System;
using TMPro;

public static class Stats
{
    public static DateTime levelStartTimeInSec;
    public static DateTime levelEndTimeInSec;

    public static int totalLettersTyped;
    public static int letterHits;

    public static float accuracy;
    public static float wpm;

    public static int points, perLevelPoints;

    public static int blueRocketCount, greenRocketCount, yellowRocketCount
        , perLevelBlueRocketCount, perLevelGreenRocketCount, perLevelYellowRocketCount;

    public static int totalWordCount, lifeLeft, wordsTyped, wordsMissed, wordsToComplete;

    public static bool restartPerformed;

    public static void CalculateStats()
    {
        levelEndTimeInSec = DateTime.Now;
        accuracy = ((float) letterHits / totalLettersTyped) * 100;
        wpm = (float)((wordsTyped / (levelEndTimeInSec - levelStartTimeInSec).TotalSeconds) * 100);
    }

    public static void ResetStats()
    {
        totalLettersTyped = letterHits = totalWordCount = wordsTyped = wordsMissed = wordsToComplete = 0;
        accuracy = wpm = 0;
        levelStartTimeInSec = DateTime.Now;
        // TODO: move to LevelData and call reset for that
        perLevelBlueRocketCount = perLevelGreenRocketCount = perLevelYellowRocketCount = 0;
        perLevelPoints = 0;
    }

    public static void DisplayStats()
    {
        GameObject accuracyValue = GameObject.Find("AccuracyValue");
        GameObject speedValue = GameObject.Find("SpeedValue");
        GameObject wordsTypedValue = GameObject.Find("WordsTypedValue");
        GameObject wordsMissedValue = GameObject.Find("WordsMissedValue");

        if (accuracyValue != null)
        {
            TextMeshProUGUI accuracyValueText = accuracyValue.GetComponent<TextMeshProUGUI>();
            accuracyValueText.text = (Double.IsNaN(accuracy) ? 0f : accuracy).ToString();
        }

        if (speedValue != null)
        {
            TextMeshProUGUI speedValueText = speedValue.GetComponent<TextMeshProUGUI>();
            speedValueText.text = wpm.ToString();
        }

        if (wordsTypedValue != null)
        {
            TextMeshProUGUI wordsTypedValueText = wordsTypedValue.GetComponent<TextMeshProUGUI>();
            wordsTypedValueText.text = wordsTyped.ToString();
        }

        if (wordsMissedValue != null)
        {
            TextMeshProUGUI wordsMissedValueText = wordsMissedValue.GetComponent<TextMeshProUGUI>();
            wordsMissedValueText.text = Math.Max(wordsToComplete - wordsTyped, 0).ToString();
        }
    }
}
