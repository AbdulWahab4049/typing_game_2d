﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MeteorSpawner : MonoBehaviour
{
    public float rockScaleFactor, rockSize;
    public int fontSize;
    public GameObject spaceRock;
    public GameObject redRock;
    public GameObject whiteRock;
    public RectTransform canvas;
    public GameManager gameManager;

    private const int BOULDER_INDEX = 0;
    private const int TEXT_INDEX = 1;
    private const float SPAWN_X_PERCENTAGE = 1.8f;
    public const float SPAWN_TOP_PERCENTAGE = 3.00f;
    public const float SPAWN_BOTTOM_PERCENTAGE = 3.00f;

    private const string SPAWN_SPACE_ROCK = "SpawnSpaceRock";
    private const int FIRST_TIME_SPAWN = 2;
    private const int NEXT_TIME_SPAWN = 7;
    private const int TOP_MARGIN = 50;
    private const int BOTTOM_MARGIN = 100;
    private const int ROCK_OVERLAP_AVOID_FACTOR = 80;
    private int wordPairsCount, arraySize;
    private string[] currentPair;
    private Vector3 newSpawnPosition;
    private GameObject newSpaceRock;
    private Quaternion spawnRotation;
    public static float screenRightLimit, screenLeftLimit, screenTopLimit, screenBottomLimit;
    private const string SPACE_STATION_TEXT = "spaceStation";
    private const string ASTRONAUGHT_TEXT = "astronaught";


    void OnEnable()
    {
        //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        CalculateScreenLimits();
        StartCoroutine(StartSpawning());
    }

    public IEnumerator StartSpawning()
    {
        yield return StartCoroutine(SettingsLoader.ReadLevelData());

        gameManager.UpdateProgressBar();
        gameManager.InitializeLifeBar(LevelData.life);

        wordPairsCount = 0;

        gameManager.InitializeProgressBar(LevelData.totalWordsCount);

        arraySize = LevelData.WordPairs.Count;

        InvokeRepeating(SPAWN_SPACE_ROCK, FIRST_TIME_SPAWN, NEXT_TIME_SPAWN);
    }

    void CalculateScreenLimits()
    {
        LogScreenResolution();

        //Right
        screenRightLimit = canvas.offsetMax.x;
        //Top
        screenTopLimit = canvas.offsetMax.y - TOP_MARGIN;
        //Bottom
        screenBottomLimit = canvas.offsetMin.y + BOTTOM_MARGIN;
        //Left
        screenLeftLimit = canvas.offsetMin.x;

        Debug.Log("Screen Top Limit: " + screenTopLimit);
        Debug.Log("Screen Bottom Limit: " + screenBottomLimit);

    }

    private static void LogScreenResolution()
    {
        Debug.Log(Screen.width + " , " + Screen.height);
    }

    private void SpawnSpaceRock()
    {
        GameObject rockToSpawn = spaceRock;

        if (wordPairsCount == arraySize || gameManager.isGameOver || gameManager.isLevelComplete)
        {
            CancelInvoke();
            if (!gameManager.isLevelComplete)
            {
                gameManager.CallGameOverIfRequired();
            }
        }
        else
        {
            rockToSpawn = SpawnSpaceRock(rockToSpawn);
        }
        wordPairsCount++;
    }

    public GameObject SpawnSpaceRock(GameObject rockToSpawn)
    {
        currentPair = (string[])LevelData.WordPairs[wordPairsCount];

        ArrayList prevPositionsForPairs = new ArrayList();

        float spawnXFactor = 0;

        foreach (string text in currentPair)
        {
            if (IsSpaceStationOrAstronaught(text))
            {
                TriggerSpaceStationOrAstronaught(text);
                continue;
            }

            string rockText = text;

            if (text.Contains("Rocket"))
            {
                TriggerRocket(text);
                continue;
            }
            else if (text.StartsWith("redRock"))
            {
                rockToSpawn = redRock;
                rockText = text.Split('|')[1];
            }
            else if (text.StartsWith("whiteRock"))
            {
                rockToSpawn = whiteRock;
                rockText = text.Split('|')[1];
            }

            DecideSpawnArea(spawnXFactor, rockText.Length);

            HandleRockPositionOverlap(prevPositionsForPairs);

            InstantiateRockWithText(rockToSpawn, rockText);

            spawnXFactor += getPercentage(screenRightLimit - screenLeftLimit, SPAWN_X_PERCENTAGE);
            prevPositionsForPairs.Add(newSpawnPosition);
        }

        return rockToSpawn;
    }

    private void InstantiateRockWithText(GameObject rockToSpawn, string rockText)
    {
        newSpaceRock = Instantiate(rockToSpawn);
        SetRockParams(rockText);

        //handling word functions
        Word word = new Word(rockText, newSpaceRock.transform.GetChild(TEXT_INDEX).GetComponent<WordDisplay>());
        gameManager.AddWord(word);
    }

    private void HandleRockPositionOverlap(ArrayList prevPositionsForPairs)
    {
        foreach (Vector3 prevSpawnPosition in prevPositionsForPairs)
        {
            //Debug.Log("OLD::::rock text: " + rockText + " , old position: " + prevSpawnPosition.ToString() + " , new position: " + newSpawnPosition.ToString());

            if (prevSpawnPosition.x > newSpawnPosition.x && (prevSpawnPosition.x - newSpawnPosition.x) < ROCK_OVERLAP_AVOID_FACTOR)
            {
                newSpawnPosition.x += (ROCK_OVERLAP_AVOID_FACTOR - (prevSpawnPosition.x - newSpawnPosition.x));
            }
            else if (newSpawnPosition.x - prevSpawnPosition.x < ROCK_OVERLAP_AVOID_FACTOR)
            {
                newSpawnPosition.x += (ROCK_OVERLAP_AVOID_FACTOR - (newSpawnPosition.x - prevSpawnPosition.x));
            }
            else if (prevSpawnPosition.y > newSpawnPosition.y && (prevSpawnPosition.y - newSpawnPosition.y) < ROCK_OVERLAP_AVOID_FACTOR)
            {
                //newSpawnPosition.y -= (ROCK_OVERLAP_AVOID_FACTOR - (prevSpawnPosition.y - newSpawnPosition.y));
                newSpawnPosition.x += (ROCK_OVERLAP_AVOID_FACTOR - (prevSpawnPosition.x - newSpawnPosition.x));

            }
            else if (newSpawnPosition.y - prevSpawnPosition.y < ROCK_OVERLAP_AVOID_FACTOR)
            {
                //newSpawnPosition.y -= (ROCK_OVERLAP_AVOID_FACTOR - (newSpawnPosition.y - prevSpawnPosition.y));
                newSpawnPosition.x += (ROCK_OVERLAP_AVOID_FACTOR - (newSpawnPosition.x - prevSpawnPosition.x));
            }

            // Debug.Log("NEW::::rock text: " + rockText + " , old position: " + prevSpawnPosition.ToString() + " , new position: " + newSpawnPosition.ToString());
        }
    }

    private bool IsSpaceStationOrAstronaught(string text)
    {
        return text.Equals(SPACE_STATION_TEXT) || text.Equals(ASTRONAUGHT_TEXT);
    }

    private void TriggerSpaceStationOrAstronaught(string text)
    {
        if (text.Equals(SPACE_STATION_TEXT))
        {
            gameManager.TriggerSpaceStation();
        }
        else
        {
            gameManager.TriggerAstronaught();
        }
    }

    public static float getPercentage(float of, float percent)
    {
        return (of / 100) * percent;
    }

    private void TriggerRocket(string text)
    {
        if (text.StartsWith("blue"))
        {
            gameManager.TriggerBlueRocket();
        }
        else if (text.StartsWith("green"))
        {
            gameManager.TriggerGreenRocket();
        }
        else if (text.StartsWith("yellow"))
        {
            gameManager.TriggerYellowRocket();
        }
    }

    private void DecideSpawnArea(float factor, int length)
    {
        newSpawnPosition = new Vector3(screenRightLimit + 30 /*+ (factor * length)*/, CalculateYPosition(), 0f);
        spawnRotation = Quaternion.identity;
    }

    public static float CalculateYPosition()
    {
        return UnityEngine.Random.Range(screenTopLimit - getPercentage(screenTopLimit - screenBottomLimit, SPAWN_TOP_PERCENTAGE)
                    , screenBottomLimit + getPercentage(screenTopLimit - screenBottomLimit, SPAWN_BOTTOM_PERCENTAGE));
    }

    private void SetRockParams(string newWord)
    {
        float scale = rockSize + newWord.Length * rockScaleFactor;
        TextMeshProUGUI word = newSpaceRock.transform.GetChild(TEXT_INDEX).GetComponent<TextMeshProUGUI>();
        word.text = newWord;
        word.fontSize = fontSize;

        newSpaceRock.transform.GetChild(0).localScale = new Vector3(scale, scale);
        newSpaceRock.transform.position = newSpawnPosition;
        newSpaceRock.transform.localRotation = spawnRotation;
        newSpaceRock.transform.SetParent(canvas);

        word.transform.SetParent(newSpaceRock.transform);

    }
}
