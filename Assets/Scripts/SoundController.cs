using UnityEngine;
using UnityEngine.SceneManagement;

public class SoundController : MonoBehaviour {
	public void onMusicToggleClick () {
        Sound.toggleMusic();
	}
	public void onSoundToggleClick () {
        Sound.toggleSoundEffects();
	}
    public void LoadScene(){
        SceneManager.LoadScene("Level 1");
    }
}