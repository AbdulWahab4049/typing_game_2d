using TMPro;
using UnityEngine;

public class WordDisplay : MonoBehaviour
{
    public TextMeshProUGUI word;
    public bool isTyped;
    public Color typedColor;
    public bool isLevelCompleteOnTyped;
    private GameManager gameManager;
    private string originalWord;

    void Start()
    {
        gameManager = GameObject.Find("Main Camera").GetComponent<GameManager>();    
    }

    public void SetWord(string text)
    {
        this.word.text = originalWord = text;
    }

    public string GetOriginalWord()
    {
        return originalWord;
    }

    public void RemoveLetter()
    {
        if (word != null)
        {
            word.text = word.text.Remove(startIndex: 0, count: 1);
            word.color = typedColor;
        }
    }

    public void RemoveWord()
    {
        if (gameObject)
        {
            isTyped = true;
            DestroyMeteor();
            Destroy(gameObject);
            if (isLevelCompleteOnTyped)
            {
                gameManager.LevelCompleteOnWhiteRockTyped();
            }
            Stats.perLevelPoints += 100;
            GameObject.Find("Points Value").GetComponent<TextMeshProUGUI>().text = (Stats.perLevelPoints + Stats.points).ToString();
        }
    }

    private void DestroyMeteor()
    {
        gameManager.PlayAudioClip(gameManager.GetRandomRockDestroySound());
        gameObject.transform.parent.gameObject.GetComponent<GUIEffects>().ApplyMoveForRandom();
        //gameObject.transform.parent.gameObject.GetComponent<GUIEffects>().ApplyScaleForRandom();
        //gameObject.transform.parent.gameObject.GetComponent<GUIEffects>().ApplyRotationForRandom();
        gameObject.transform.parent.gameObject.GetComponent<GUIEffects>().isFade = true;
        gameObject.transform.parent.gameObject.GetComponent<GUIEffects>().isDestroyOnDismiss= true;
        gameObject.transform.parent.gameObject.GetComponent<GUIEffects>().Start();
    }

}
