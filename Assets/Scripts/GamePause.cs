﻿using System.Collections;
using UnityEngine;

public class GamePause : MonoBehaviour {

    [Range(0f, 1.5f)]
    public float duration = 1f;

    private Animator freezeAnimator;

    bool _isFrozen = false;

    float _pendingFreezeDuration = 0f;

    void Start()
    {
        freezeAnimator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_pendingFreezeDuration > 0 && !_isFrozen)
        {
            StartCoroutine("DoFreeze");
            _pendingFreezeDuration=0;
            freezeAnimator.SetBool("DoFreeze", false);
        }
    }

    public void Freeze(float duration)
    {
        _pendingFreezeDuration = duration;
    }

    IEnumerator DoFreeze()
    {
        freezeAnimator.SetBool("DoFreeze", true);
        yield return new WaitForSeconds(1f);
    }
}
