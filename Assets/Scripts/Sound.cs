public static class Sound
{
    private static bool isMusicEnabled;
    private static bool isSoundEnabled;

    public static void toggleMusic()
    {
        isMusicEnabled = !isMusicEnabled;
    }

    public static void toggleSoundEffects()
    {
        isSoundEnabled = !isSoundEnabled;
    }

    public static bool getMusicEnabled()
    {
        return isMusicEnabled;
    }

    public static bool getSoundEffectEnabled()
    {
        return isSoundEnabled;
    }

    public static void resetSettings()
    {
        isMusicEnabled = false;
        isSoundEnabled = false;
    }
}
