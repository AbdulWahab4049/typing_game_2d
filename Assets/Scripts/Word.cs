using System;

[System.Serializable]
public class Word {

	private string word;
	private int typeIndex;
    private bool destroyed;

	WordDisplay display;

	public Word (string _word, WordDisplay _display)
	{
		word = _word;
		typeIndex = 0;

		display = _display;
        if (display != null)
        {
            display.SetWord(word);
        }
    }

    public string GetText()
    {
        return word;
    }

	public char GetNextLetter ()
	{
        if (typeIndex < word.Length)
        { 
		    return word[typeIndex];
        }
        return ' ';
    }

	public void TypeLetter ()
	{
		typeIndex++;
        if (display != null)
        {
            display.RemoveLetter();
        }
    }

	public bool WordTyped ()
	{
		bool wordTyped = (typeIndex >= word.Length);
		return wordTyped;
	}

    public void RemoveWord()
    {
        if (display != null)
        { 
            display.RemoveWord();
            destroyed = true;

        }
    }

    public bool IsDestroyed()
    {
        return destroyed;
    }

    internal void setDestroyed(bool value)
    {
        destroyed = value;
    }
}
