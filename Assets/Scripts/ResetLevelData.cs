﻿using UnityEngine;

public class ResetLevelData : MonoBehaviour
{
    void Start()
    {
        LevelData.Reset();
        Stats.points = 0;
        Cursor.visible = true;
    }
}
