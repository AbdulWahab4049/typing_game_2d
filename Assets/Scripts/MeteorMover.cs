﻿using UnityEngine;

public class MeteorMover : MonoBehaviour
{
    public bool isGameOverOnMiss;

    private GameManager gameManager;

    private static float actualSpeed;
    private static bool isYellowRocketActivated;
    private float screenLeftLimit;
    private const float LEFT_BOUND_FACTOR = 1.05f;

    void Start()
    {
        gameManager = GameObject.Find("Main Camera").GetComponent<GameManager>();

        actualSpeed = Random.Range(LevelData.minSpeed, LevelData.maxSpeed);

        if (isYellowRocketActivated)
        {
            actualSpeed /= 2;
        }
    }

    void Update()
    {
        if (transform.position.x > screenLeftLimit * LEFT_BOUND_FACTOR) //rock can be typed
            transform.Translate(Vector2.left * actualSpeed * Time.deltaTime, Space.World);
        else // rock missed
        {
            if (!gameManager.isGameOver && !gameManager.isLevelComplete)
            {
                gameManager.PlayRockMissedAudioClip();
            }

            if (isGameOverOnMiss)
            {
                gameManager.GameOverOnRedRockMissed();
            }
            else
            {
                ResetActiveWordIfRequired();
                gameManager.UpdateLifeBar();
            }
            gameManager.RemoveWord(gameObject.GetComponentInChildren<WordDisplay>().GetOriginalWord());
            Destroy(gameObject);
        }
    }

    private void ResetActiveWordIfRequired()
    {
        string activeWordText = gameManager.GetActiveWordText();
        if (activeWordText != null && gameObject.GetComponentInChildren<WordDisplay>() != null
        && activeWordText.Equals(gameObject.GetComponentInChildren<WordDisplay>().GetOriginalWord()))
        {
            Debug.Log("resetting active word");
            gameManager.ResetActiveWord();
        }
    }

    public static void SlowActualSpeedToHalf()
    {
        actualSpeed /= 2;
    }

    public static void IncreaseActualSpeedToDouble()
    {
        actualSpeed *= 2;
    }

    public static bool IsYellowRocketActivated()
    {
        return isYellowRocketActivated;
    }

    public static void SetYellowRocketActivated(bool isActivated)
    {
        isYellowRocketActivated = isActivated;
    }
}
