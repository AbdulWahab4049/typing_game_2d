﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class MainMenuInstructionsPanelHandler : MonoBehaviour
{
	public void onInstructionsButtonClick () {
        SceneManager.LoadScene("Instructions");
	}

	public void onBackToMainMenuClick () {
        SceneManager.LoadScene("MainMenu");
	}
}